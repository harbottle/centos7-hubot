FROM centos:latest
MAINTAINER harbottle
RUN yum -y install epel-release
RUN yum -y localinstall https://harbottle.gitlab.io/ergel/7/x86_64/ergel-release-7-2.el7.x86_64.rpm
RUN yum -y install rubygems
RUN yum -y install ruby-devel
RUN yum -y install rpm-build
RUN yum -y install rubygem-POpen4
RUN yum -y install rubygem-colorize
RUN yum -y install rubygem-fpm
RUN yum -y install rubygem-rpm
RUN yum -y install gcc
RUN yum -y install gcc-c++
RUN yum -y install make
RUN yum -y install nodejs
RUN yum -y install createrepo
RUN npm install -g "yo"
RUN npm install -g "generator-hubot"
RUN npm install -g "hubot"
