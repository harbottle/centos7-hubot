require 'colorize'
require 'date'
require 'fileutils'
require 'fpm'
require 'json'
require 'popen4'
require 'yaml'

pad_length = 60
$stdout.sync = true

# Function to convert (yaml) object to hash with symbol keys
def symbolize(obj)
    return obj.inject({}){|memo,(k,v)| memo[k.to_sym] =  symbolize(v); memo} if obj.is_a? Hash
    return obj.inject([]){|memo,v    | memo           << symbolize(v); memo} if obj.is_a? Array
    return obj
end

# Load YAML file
print "Loading 'hubot.yml'".ljust(pad_length, padstr='.')
begin
  hubot = symbolize(YAML::load_file(File.join(Dir.pwd, 'hubot.yml')))
rescue StandardError => error 
  print "Failure:\n#{error.to_s.strip}\n".red
  exit 2
else
  print "Success\n".green
end

# Ensure dirs are created and have correct permissions to allow bot to build
bot_dir = File.join(Dir.pwd, 'bot')
FileUtils.mkdir_p("#{bot_dir}")
FileUtils.mkdir_p("/root/.config/configstore")
FileUtils.mkdir_p("/root/.npm/hubot-rules")
FileUtils.mkdir_p("/root/.npm/_locks/")
FileUtils.chmod_R("g+rwx", "/root")
FileUtils.chmod_R("g+rwx", "#{bot_dir}")

# Build bot
command = "cd '#{bot_dir}' && yo hubot --no-insight --owner='#{ENV['CI_PROJECT_NAMESPACE']}' --name='#{hubot[:name]}' --description='#{hubot[:description]}' --adapter='#{hubot[:adapter]}' --defaults"
print "Building #{hubot[:name]}".ljust(pad_length, padstr='.')
print "\n"
command_status = POpen4::popen4(command) do |stdout, stderr, stdin|
  stdout.each do |line|
    print "    #{line}".light_blue
  end
  stderr.each do |line|
    print "    #{line}".light_blue
  end
end
print ".".rjust(pad_length, padstr='.')
if command_status.exitstatus == 0
  print "Success".green
  print "\n\n\n"
else
  print "Failure".red
  exit command_status.exitstatus
end

# Add external scripts to bot
hubot[:external_scripts].each do |external_script|
  command_desc = "Installing hubot external script '#{external_script}'".ljust(pad_length, padstr='.')
  command = "cd '#{bot_dir}' && npm install '#{external_script}' --save"
  print "#{command_desc}\n"
  command_status = POpen4::popen4(command) do |stdout, stderr, stdin|
    stdout.each do |line|
      print "    #{line}".light_blue
    end
    stderr.each do |line|
      print "    #{line}".red
    end
  end
  print ".".rjust(pad_length, padstr='.')
  if command_status.exitstatus == 0
    print "Success".green
    print "\n\n\n"
  else
    print "Failure".red
    exit command_status.exitstatus
  end
end
print "Writing external-scripts.json file".ljust(pad_length, padstr='.')
begin
  default_external_scripts = JSON.parse(File.read("#{bot_dir}/external-scripts.json"))
  File.open("#{bot_dir}/external-scripts.json", "w") do |f|
    f.write((default_external_scripts + hubot[:external_scripts]).to_json)
  end
rescue StandardError => error 
  print "Failure:\n#{error.to_s.strip}\n".red
  exit 2
else
  print "Success\n".green
end

print "Removing redundant hubot-scripts.json file".ljust(pad_length, padstr='.')
begin
  FileUtils.rm_f("#{bot_dir}/hubot-scripts.json")
rescue StandardError => error 
  print "Failure:\n#{error.to_s.strip}\n".red
  exit 2
else
  print "Success\n".green
end

# Define package attributes
time = Time.new
pkg = FPM::Package::Dir.new
pkg.name         = "#{hubot[:name]}"
pkg.version      = "#{time.gmtime.strftime('%Y%m%d%H%M%S')}"
pkg.iteration    = '1.el7'
pkg.description  = "#{hubot[:description]}"
pkg.maintainer   = "#{ENV['CI_PROJECT_NAMESPACE']}"
pkg.vendor       = "#{ENV['CI_PROJECT_NAMESPACE']}"
pkg.license      = 'MIT'
pkg.url          = "#{ENV['CI_PROJECT_URL']}"
pkg.config_files = ["/etc/#{hubot[:name]}.conf"]
pkg.dependencies = ['nodejs', 'redis']
pkg.scripts[:before_install] = <<-SCRIPT.gsub(/^ {2}/, '')
  getent group #{hubot[:name]} &>/dev/null || groupadd -r #{hubot[:name]}
  getent passwd #{hubot[:name]} &>/dev/null ||
  useradd -r -g #{hubot[:name]} -d /opt/#{hubot[:name]} -s /bin/bash -c "#{hubot[:name]} system account" #{hubot[:name]}
  usermod -g #{hubot[:name]} -d /opt/#{hubot[:name]} -c "#{hubot[:name]} system account" #{hubot[:name]} &>/dev/null
  exit 0
SCRIPT
pkg.scripts[:after_install] = <<-SCRIPT.gsub(/^ {2}/, '')
  chown -R #{hubot[:name]}:#{hubot[:name]} /opt/#{hubot[:name]}
  chmod 0600 /etc/#{hubot[:name]}
  systemctl daemon-reload
SCRIPT
pkg.scripts[:after_remove] = 'systemctl daemon-reload'

# Add bot files to package
pkg.attributes[:prefix] = "/opt/#{hubot[:name]}"
pkg.attributes[:chdir]  = "#{bot_dir}"
pkg.input('.')

# Add conf file to package
file = File.open("/tmp/#{hubot[:name]}.conf", "w")
hubot[:environment].each do |key, value|
  file.write("#{key}=#{value}\n")
end
file.close
pkg.attributes[:prefix] = '/etc'
pkg.attributes[:chdir]  = '/tmp'
pkg.input("#{hubot[:name]}.conf")

# Add systemd service file to package
file = File.open("/tmp/#{hubot[:name]}.service", "w")
file.write(<<-SERVICE.gsub(/^ {2}/, ''))
  [Unit]
  Description=#{hubot[:name]}
  After=syslog.target network.target

  [Service]
  EnvironmentFile=/etc/#{hubot[:name]}.conf
  Type=simple
  WorkingDirectory=/opt/#{hubot[:name]}
  ExecStart=/usr/bin/node /opt/#{hubot[:name]}/node_modules/.bin/coffee /opt/#{hubot[:name]}/node_modules/.bin/hubot -a #{hubot[:adapter]} --name "#{hubot[:name]}"
  PIDFile=/var/spool/#{hubot[:name]}/pid/master.pid
  User=#{hubot[:name]}
  Group=#{hubot[:name]}
  Restart=always
  StandardOutput=syslog
  StandardError=syslog
  SyslogIdentifier=#{hubot[:name]}

  [Install]
  WantedBy=multi-user.target
SERVICE
file.close
pkg.attributes[:prefix] = '/lib/systemd/system'
pkg.attributes[:chdir]  = '/tmp'
pkg.input("#{hubot[:name]}.service")

# Create RPM
output = 'NAME-VERSION-ITERATION.ARCH.rpm'
print "Building RPM file".ljust(pad_length, padstr='.')
begin
  rpm = pkg.convert(FPM::Package::RPM)
  rpm.output(rpm.to_s(output))
rescue StandardError => error 
  print "Failure:\n#{error.to_s.strip}\n".red
  exit 3
else
  print "Success.\n".green
end
