# centos7-hubot
[![build status](https://gitlab.com/harbottle/centos7-hubot/badges/master/build.svg)](https://gitlab.com/harbottle/centos7-hubot/builds)

Build a customized Hubot RPM for CentOS 7 using GitLab CI.
