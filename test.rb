require 'colorize'
require 'date'
require 'fileutils'
require 'popen4'
require 'rpm'
require 'yaml'

pad_length = 60
$stdout.sync = true
global_status = 0

# Function to convert (yaml) object to hash with symbol keys
def symbolize(obj)
    return obj.inject({}){|memo,(k,v)| memo[k.to_sym] =  symbolize(v); memo} if obj.is_a? Hash
    return obj.inject([]){|memo,v    | memo           << symbolize(v); memo} if obj.is_a? Array
    return obj
end

# Load YAML file
print "Loading 'hubot.yml'".ljust(pad_length, padstr='.')
begin
  hubot = symbolize(YAML::load_file(File.join(Dir.pwd, 'hubot.yml')))
rescue StandardError => error 
  print "Failure:\n#{error.to_s.strip}\n".red
  exit 2
else
  print "Success\n".green
end

# Build and write local testing repo file
File.write('/etc/yum.repos.d/local-testing-repo.repo', <<"CONF"
[local-testing-repo]
name=local-testing-repo
baseurl=file://#{ENV['CI_PROJECT_DIR']}
gpgcheck=0
enabled=1
CONF
)

commands = {
  'Creating local yum repo.......' => 'createrepo -d .',
  'Clearing yum cache............' => 'yum clean all' }

commands.each do |key, value|
  command_desc = key
  command = value
  print "#{command_desc}".ljust(pad_length, padstr='.')
  outputs = ''
  errors =  ''
  command_status = POpen4::popen4(command) do |stdout, stderr, stdin|
    stdout.each do |line|
      outputs << "#{line.strip}\n"
    end
    stderr.each do |line|
      errors << "#{line.strip}\n"
    end
  end
  if command_status.exitstatus == 0
    print "Success\n".green
  else
    print "Failure:\n".red
    print "#{outputs.strip}\n".red 
    print "#{errors.strip}\n".red 
    global_status = command_status.exitstatus
  end
end


print "Testing for #{hubot[:name].to_s.light_blue} RPM...\n"
file_name = ''
file_error = ''

# File exists?
print "  File exists?".ljust(pad_length, padstr='.')
dir_result = Dir["#{hubot[:name]}-[0-9]*.rpm"]
if dir_result.empty?
  print "Failure: #{hubot[:name]} RPM does not exist.\n".red
  global_status += 1
else
  file_name = dir_result[0]
  print "Success: '#{file_name}'\n".green

  # RPM testing
  pkg = RPM::Package.open(file_name)

  # RPM has a name?
  print "  RPM has a name?".ljust(pad_length, padstr='.')
  begin
    pkg_name = pkg.name
  rescue StandardError => error
  end
  if pkg_name.nil?
    print "Failure: #{file_name} has no name. #{error.to_s.strip}\n".red
    global_status += 1
  else
    print "Success: '#{pkg_name}'.\n".green
  end

  # RPM has a version?
  print "  RPM has a version?".ljust(pad_length, padstr='.')
  begin
    pkg_version = pkg.version.v
  rescue StandardError => error
  end
  if pkg_version.nil?
    print "Failure: #{file_name} has no version. #{error.to_s.strip}\n".red
    global_status += 1
  else
    print "Success: '#{pkg_version}'.\n".green
  end

  #RPM has an iteration?
  print "  RPM has an iteration?".ljust(pad_length, padstr='.')
  begin
    pkg_iteration = pkg.version.r
  rescue StandardError => error
  end
  if pkg_iteration.nil?
    print "Failure: #{file_name} has no iteration. #{error.to_s.strip}\n".red
    global_status += 1
  else
    print "Success: '#{pkg_iteration}'.\n".green
  end

  #RPM contains files?
  print "  RPM contains files?".ljust(pad_length, padstr='.')
  begin
    pkg_files = pkg.files
  rescue StandardError => error
  end
  if pkg_files.nil?
    print "Failure: #{file_name} has no file count. #{error.to_s.strip}\n".red
    global_status += 1
  else
    if pkg_files.count == 0
      print "Warning: #{file_name} has file count of 0.\n".yellow
    else
      print "Success: #{pkg_files.count} files counted.\n".green
    end
  end

  #RPM contains dependenices?
  print "  RPM contains dependencies?".ljust(pad_length, padstr='.')
  begin
    pkg_requires = pkg.requires
  rescue StandardError => error
  end
  if pkg_requires.nil?
    print "Failure: #{file_name} has no dependencies. #{error.to_s.strip}\n".red
    global_status += 1
  else
    dependencies = []
    pkg.requires.each do |dependency|
      dependencies << { :name => dependency.name, :version => dependency.version.v }
    end
    print  "Success:".green
    dependencies.each do | dependency |
      print "\n    "
      print "#{dependency[:name]}".ljust(40).light_blue
      unless dependency[:version] == ""
        print " >= "
        print "#{dependency[:version]}".light_blue
      end
    end
    print "\n"
  end
  
  #RPM provides?
  print "  RPM provides?".ljust(pad_length, padstr='.')
  begin
    pkg_provides = pkg.provides
  rescue StandardError => error
  end
  if pkg_provides.nil?
    print "Failure: #{file_name} provides nothing. #{error.to_s.strip}\n".red
    global_status += 1
  else
    provides = []
    pkg.provides.each do |provide|
      #provides << provide.name
      provides << { :name => provide.name, :version => provide.version.v }
    end
    print  "Success:".green
    provides.each do | provide |
      print "\n    "
      print "#{provide[:name]}".ljust(40).light_blue
      unless provide[:version] == ""
        print " >= "
        print "#{provide[:version]}".light_blue
      end
    end
    print "\n"
  end

  #RPM installs via yum?
  command = "yum install -y #{hubot[:name]}"
  print "  RPM installs using yum?".ljust(pad_length, padstr='.')
  outputs = ''
  errors =  ''
  command_status = POpen4::popen4(command) do |stdout, stderr, stdin|
    stdout.each do |line|
      outputs << "#{line.strip}\n"
    end
    stderr.each do |line|
      errors << "#{line.strip}\n"
    end
  end
  if command_status.exitstatus == 0
    print "Success: Installed.\n".green
  else
    print "Failure:\n".red
    print "#{outputs.strip}\n".red 
    print "#{errors.strip}\n".red 
    global_status = command_status.exitstatus
  end
  
  #RPM is installed?
  command = "yum list installed #{hubot[:name]}"
  print "  RPM is installed?".ljust(pad_length, padstr='.')
  outputs = ''
  errors =  ''
  command_status = POpen4::popen4(command) do |stdout, stderr, stdin|
    stdout.each do |line|
      outputs = "#{line.strip}"
    end
    stderr.each do |line|
      errors << "#{line.strip}\n"
    end
  end
  if command_status.exitstatus == 0
    print "Success: #{outputs.squeeze(" ")}\n".green
  else
    print "Failure:\n".red
    print "#{outputs.strip}\n".red 
    print "#{errors.strip}\n".red 
    global_status = command_status.exitstatus
  end  
end

# Hubot runs?
command = "while read p; do echo $p; export $p; done < /etc/'#{hubot[:name]}'.conf; cd /opt/'#{hubot[:name]}'; echo $PATH; /usr/bin/node /opt/'#{hubot[:name]}'/node_modules/.bin/coffee /opt/'#{hubot[:name]}'/node_modules/.bin/hubot -a shell --name '#{hubot[:name]}'"
print "Running bot using shell adapter".ljust(pad_length, padstr='.')
print "\n"
puts command
command_status = POpen4::popen4(command) do |stdout, stderr, stdin|
  hubot[:test_commands].each do |command|
    stdin.puts "\n"
    stdin.puts "#{command}"
    stdin.puts "\n"
  end
  stdin.puts 'exit'
  stdout.each do |line|
    print "    #{line}".light_blue
  end
  stderr.each do |line|
    print "    #{line}".light_blue
  end
end
print ".".rjust(pad_length, padstr='.')
if command_status.exitstatus == 0
  print "Success".green
  print "\n\n\n"
else
  print "Failure".red
  exit command_status.exitstatus
end

exit global_status